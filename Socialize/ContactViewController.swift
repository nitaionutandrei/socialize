import UIKit

class ContactViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var usernameField: UITextField!

    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var descriptionTextView: UITextView!    

    @IBAction func passwordActionTriggered(_ sender: AnyObject) {
        print ("Your username is: \(usernameField.text!)")
        print ("Your password is: \(passwordField.text!)")
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        
        self.usernameField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        
    }
    
}
