import UIKit
import CoreData

class JokesTableViewController: UITableViewController {
    
    var jokes : Array<Dictionary<String, AnyObject>>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initJokes()
        // requestJokes()
        // storeInUserDefaults()
        // storeTranscription()
        getTranscriptions()
        
    }
    
    func storeInUserDefaults() {
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        print (documentsPath)
        
        let userDefaults = UserDefaults.standard
        
        let storedTranscriptions = userDefaults.value(forKey: "Transcriptions")
        
        if storedTranscriptions != nil {
            
            print (storedTranscriptions)
            
            print ("Loaded!")
            
        } else {
            
            let transcriptions = self.jokes!
            
            userDefaults.set(transcriptions, forKey: "Transcriptions")
            
            print (transcriptions)
            
            print ("Saved!")
            
        }
        
    }
    
    func getTranscriptions() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Jokes")
        
        do {
            let searchResults = try getContext().fetch(fetchRequest)
            print ("number of results = \(searchResults.count)")
            for joke in searchResults as! [NSManagedObject] {
                print ("joke: \(joke)")
            }
        }catch {
                print ("Error with request: \(error)")
        }
    }
    
    func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
        func storeTranscription() {
            let context = getContext()
            let entity = NSEntityDescription.entity(forEntityName: "Jokes", in: context)
            let transcriptions = self.jokes!
            for jokesDict in transcriptions {
                print (jokesDict)
                let transc = NSManagedObject(entity: entity!, insertInto: context)
                transc.setValue(jokesDict["id"], forKey: "id")
                transc.setValue(jokesDict["text"], forKey: "text")
                transc.setValue(jokesDict["source"], forKey: "source")
            }
            do {
                try context.save()
                print ("Saved!")
            } catch let error as NSError {
                print ("could not save \(error), \(error.userInfo)")
            }
        }
    
//    func storedTranscription() {
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Jokes", into: context)
//        print (newUser)
//        newUser.setValue("1", forKey: "id")
//        
//        newUser.setValue("Chuck Norris once fought Superman. The loser had to wear his underwear over his pants.", forKey: "text")
//        newUser.setValue("http://jokes.cc.com/funny-marriage/lgn87x/danny-bevins--marriage-is-not-a-job", forKey: "source")
//        
//        do {
//         try context.save()
//            print ("Saved!")
//        }catch{
//            //pass
//        }
//        
//    }
    
    func initJokes() {
        
        if let path = Bundle.main.path(forResource: "Jokes",
                                       ofType: "json") {
            
            do {
                
                let content = try String(contentsOfFile: path)
                processJSON(content)
                
            } catch let contentError {
                print(contentError)
            }
        }
    }
    
    func requestJokes() {
        
        
        let address = "http://localhost:8000/Jokes.json"
        //        let address = "http://api.icndb.com/jokes/random"
        let session = URLSession.shared
        
        let url = URL(string: address)
        
        let dataTask = session.dataTask(with: url!, completionHandler: {
            (data, response, error) -> Void in
            
            let content = String(data: data!, encoding:String.Encoding.utf8)
            if content != nil {
                self.processJSON(content!)
            }
        })
        
        dataTask.resume()
    }
    
    func processJSON(_ content: String) {
        
        let data = content.data(using: String.Encoding.utf8)
        
        do {
            
            let json = try JSONSerialization.jsonObject(with: data!,
                                                        options: JSONSerialization.ReadingOptions.allowFragments)
            
            if JSONSerialization.isValidJSONObject(json) {
                self.reloadTableWith(json: json)
            }
            
        } catch let jsonError {
            print("jsonError :\(jsonError)")
        }
    }
    
    func reloadTableWith(json: Any) {
        self.jokes = json as? Array<Dictionary<String, AnyObject>>
        
        if self.jokes != nil {
            
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()
            })
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if jokes != nil {
            return jokes!.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        let currentCell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        print (" Joke \(row): " + "\(currentCell.textLabel!.text!)")
    }
    
    static let CellIdentifier = "JokesIdentifier"
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: JokesTableViewController.CellIdentifier)
        
        let joke = self.jokes?[indexPath.row]
        if joke != nil {
            let text = joke!["text"] as? String
            let subtitle = joke!["source"] as? String
            
            cell?.textLabel?.text = text
            cell?.detailTextLabel?.text = subtitle
        }
        
        return cell!
    }
    
}
