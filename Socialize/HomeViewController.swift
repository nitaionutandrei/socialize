import UIKit

class HomeViewController: UIViewController , UIScrollViewDelegate {
 
    @IBOutlet weak var scrollView :UIScrollView!
    
    var page = 0
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.initScrollGallery()
    }

    private func initScrollGallery() {
        
        self.scrollView.isPagingEnabled = true

        let images = ["Pic1", "Pic2", "Pic3"]
        
        let scrollViewBounds = self.scrollView.bounds

        for i in 0..<images.count {

            let imageView = UIImageView(
                frame: CGRect(
                    x: 0, // fix
                    y: scrollViewBounds.size.height * CGFloat(i), // variabil
                    width:scrollViewBounds.size.width, // fix
                    height: scrollViewBounds.size.height // fix
                )
            )

            imageView.image = UIImage(named: images[i])
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(HomeViewController.tappedGesture)
            )
            tapGesture.numberOfTapsRequired = 2

            imageView.isUserInteractionEnabled = true

            imageView.addGestureRecognizer(tapGesture)
            self.scrollView.addSubview(imageView)
        }

        self.scrollView.contentSize = CGSize(
            width: scrollViewBounds.size.width,
            height: scrollViewBounds.size.height * CGFloat(images.count)
        )
    }
    
    func tappedGesture() {
        print("Sunt la pagina: \(self.page)")
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("Hello there")
        
        let y = scrollView.contentOffset.y
        let height = scrollView.bounds.size.height
        
        print(y/height)
        
        self.page = Int(floor(y/height))
    }

}

